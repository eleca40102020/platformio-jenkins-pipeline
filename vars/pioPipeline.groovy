def call(Map config) {
    def buildEnv = config.buildEnv

    pipeline {
        agent { label 'platformio-docker' }
        options {
            timeout(time: 60, unit: 'MINUTES')
        }

        stages {
            stage('Run unit tests') {
                steps {
                    sh 'echo "Unimplemented"'
                    /*
                    sh 'pio test -e ${buildEnv}'
                    */
                }
            }
            stage('Static code analysis') {
                steps {
                    sh "pio check -e ${buildEnv}"
                }
            }
            stage('Build software') {
                steps {
                    sh "pio run -e ${buildEnv}"
                }
            }
            stage('Archive artifacts') {
                when {
                    expression { GIT_BRANCH == 'master' }
                }
                steps {
                    script {
                        rtUpload (
                            serverId: "${config.artifactoryServerId}",
                            specPath: 'artifactFilespec.json',
                            failNoOp: true
                        )
                        rtPublishBuildInfo (
                            serverId: "${config.artifactoryServerId}",
                        )
                    }
                }
            }
        }
    }
}

